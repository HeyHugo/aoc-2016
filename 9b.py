import re
import time

MARKER_REGEX = re.compile(r'^\((\d+)x(\d+)')

def decompress(data):
    count = 0
    while len(data) > 0:
        if data[0] != '(':
            data = data[1:]
            count += 1
            continue

        marker_match = MARKER_REGEX.search(data)
        (offset, multiplier) = marker_match.groups()
        marker_length = 3 + len(offset) + len(multiplier)
        offset = int(offset)
        multiplier = int(multiplier)
        data = data[marker_length:]
        count += decompress(data[:offset]) * multiplier
        data = data[offset:]

    return count

if __name__ == '__main__':
    with open('9-input.txt', 'r') as f:
        data = f.read()
    data = data.strip()
    t1 = time.time()
    count = decompress(data)
    t2 = (time.time() - t1) * (1000.0)
    print("Length: %d" % (count))
    print("Solve time: %dms" % (t2))
