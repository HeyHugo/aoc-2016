import re

if __name__ == '__main__':
    with open('12-input.txt', 'r') as f:
        data = f.read()
    lines = data.strip().split('\n')

    registers = {key: 0 for key in 'abcd'}
    registers['c'] = 1
    i = 0
    while i < len(lines):
        line = lines[i]
        if i == 9:
            print registers
            print line
        #print line
        #print i
        op = line[:3]

        if op == 'cpy':
            m = re.match(r'cpy (\d+|[abcd]) (\d+|[abcd])', line)
            (source, dest) = m.groups()
            if source in 'abcd':
                registers[dest] = registers[source]
            else:
                registers[dest] = int(source)

        if op == 'inc':
            registers[line[-1:]] += 1

        if op == 'dec':
            registers[line[-1:]] -= 1

        if op == 'jnz':
            m = re.match(r'jnz (\d+|[abcd]) (-?\d+|[abcd])', line)
            (check_zero, jump) = m.groups()
            jump = int(jump)
            if check_zero in 'abcd':
                check_zero = registers[check_zero]
            else:
                check_zero = int(check_zero)
            if check_zero != 0:
                if i + jump > len(lines)-1:
                    print register
                    exit(0)

                i += jump
                continue
        i += 1

    print registers['a']
