if __name__ == '__main__':

    with open('6-input.txt', 'r') as f:
        data = f.read()
    lines = data.strip().split('\n')
    chars = [{},{},{},{},{},{},{},{}]
    for line in lines:
        for i, char in enumerate(line):
            count = chars[i].get(char, 0)
            chars[i][char] = count+1

    print("First problem:")
    for c in chars:
        print max(c.items(), key=lambda x: x[1])

    print('\nSeconds problem:')
    for c in chars:
        print min(c.items(), key=lambda x: x[1])
