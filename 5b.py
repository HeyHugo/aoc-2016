from hashlib import md5
import time

INPUT = 'reyedfim'

if __name__ == '__main__':
    t1 = time.time()
    i = 0
    password = {}
    while len(password) < 8:
        h = md5()
        h.update(INPUT + str(i))
        digest = h.hexdigest()
        if digest[:5] == '00000':
            print digest[5:7]
            pos = int(digest[5:6], 16)
            if pos < 8 and password.get(pos) is None:
                password[pos] = digest[6:7]
        i += 1

    print password.values()
    t2 = time.time() - t1
    print('Time: %f' % (t2))
