import re

def has_abba(s):
    length = len(s)
    for i in range(length):
        if i > (length - 4):
            return False
        if s[i] == s[i+3] and s[i+1] == s[i+2] and s[i] != s[i+1]:
            return True

def bab_pairs(s):
    length = len(s)
    pairs = []
    for i in range(length):
        if i > (length - 3):
            return pairs
        if s[i] == s[i+2] and s[i] != s[i+1]:
            pairs.append(s[i:i+2])
    return pairs


if __name__ == '__main__':
    tls_count = 0
    ssl_count = 0

    with open('7-input.txt', 'r') as f:
        data = f.read()

    lines = data.strip().split('\n')
    for line in lines:
        segments = re.findall(r"(?:^|\])(.*?)(?:\[|$)", line)
        hypernets = re.findall(r"\[(.*?)\]", line)

        valid_segments = False
        valid_hypernets = True
        for s in segments:
            if has_abba(s):
                valid_segments = True
                break

        for h in hypernets:
            if has_abba(h):
                valid_hypernets = False

        if valid_hypernets is True and  valid_segments is True:
            tls_count += 1

        ssl_hypernets = [bab_pairs(h) for h in hypernets]
        ssl_hypernets = [h for sublist in ssl_hypernets for h in sublist]

        ssl_segments = [bab_pairs(s) for s in segments]
        ssl_segments = [s for sublist in ssl_segments for s in sublist]

        for pair in ssl_segments:
            if pair[::-1] in ssl_hypernets:
                ssl_count += 1
                break

    print("TLS count: %d" % (tls_count))
    print("SSL count: %d" % (ssl_count))
