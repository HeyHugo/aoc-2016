

INPUT = "R4, R4, L1, R3, L5, R2, R5, R1, L4, R3, L5, R2, L3, L4, L3, R1, R5, R1, L3, L1, R3, L1, R2, R2, L2, R5, L3, L4, R4, R4, R2, L4, L1, R5, L1, L4, R4, L1, R1, L2, R5, L2, L3, R2, R1, L194, R2, L4, R49, R1, R3, L5, L4, L1, R4, R2, R1, L5, R3, L5, L4, R4, R4, L2, L3, R78, L5, R4, R191, R4, R3, R1, L2, R1, R3, L1, R3, R4, R2, L2, R1, R4, L5, R2, L2, L4, L2, R1, R2, L3, R5, R2, L3, L3, R3, L1, L1, R5, L4, L4, L2, R5, R1, R4, L3, L5, L4, R5, L4, R5, R4, L3, L2, L5, R4, R3, L3, R1, L5, R5, R1, L3, R2, L5, R5, L3, R1, R4, L5, R4, R2, R3, L4, L5, R3, R4, L5, L5, R4, L4, L4, R1, R5, R3, L1, L4, L3, L4, R1, L5, L1, R2, R2, R4, R4, L5, R4, R1, L1, L1, L3, L5, L2, R4, L3, L5, L4, L1, R3"


if __name__ == '__main__':
    places = []
    coord = [0, 0]
    direction = 0
    dirs = [
        (0,1),
        (1,0),
        (0,-1),
        (-1,0)
    ]
    actions = INPUT.replace(' ', '').split(',')
    for a in actions:
        move = a[:1]
        if move == 'R':
            direction  = (direction + 1) % 4
        else:
            direction  = (direction - 1) % 4

        speed = int(a[1:])
        for s in range(speed):
            coord = [coord[0] + dirs[direction][0], coord[1] + dirs[direction][1]]
            places.append(coord)

    distance = abs(coord[0]) + abs(coord[1]);
    print("Distance to goal: %s" % distance)
    doubles = [p for p in places if places.count(p) > 1]
    first_double_distance = abs(doubles[0][0]) + abs(doubles[0][1])
    print("First intersection: %d" % first_double_distance)
