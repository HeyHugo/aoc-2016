import re


def is_triangle(x,y,z):
    if x + y <= z or x + z <= y  or y + z <= x:
        return False
    return True


if __name__ == '__main__':
    shapes = []
    count = 0
    with open('3-input.txt', 'r') as f:
        data = f.read()

    lines = data.strip().split('\n')
    for i, line in enumerate(lines):
        shape = re.findall(r'(\d+)', line)
        shape = [int(s) for s in shape]
        if i % 3 == 0:
            for s in shape:
                shapes.append([s])
        else:
            end = len(shapes)-1
            shapes[end-2].append(shape[0])
            shapes[end-1].append(shape[1])
            shapes[end].append(shape[2])


    for s in shapes:
        if is_triangle(*s):
            count = count + 1

    print count
