import re


def is_triangle(x,y,z):
    if x + y <= z or x + z <= y  or y + z <= x:
        return False
    return True


if __name__ == '__main__':
    shapes = []
    count = 0
    with open('3-input.txt', 'r') as f:
        data = f.read()

    lines = data.strip().split('\n')
    for line in lines:
        shape = re.findall(r'(\d+)', line)
        shape = [int(s) for s in shape]
        shapes.append(shape)

    for s in shapes:
        if is_triangle(*s):
            count = count + 1

    print count
