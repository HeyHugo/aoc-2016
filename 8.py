import re

class Screen():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.pixels = [['.' for row in range(x)] for col in range(y)]

    def create_rect(self, x, y):
        x = int(x)-1
        y = int(y)-1
        for i in range(int(y)+1):
            for j in range(int(x)+1):
                self.pixels[i][j] = '#'

    def rotate_row(self, i, n):
        n = int(n) % self.x
        i = int(i)
        self.pixels[i] = self.pixels[i][-n:] + self.pixels[i][:-n]

    def rotate_column(self, i, n):
        n = int(n) % self.y
        i = int(i)
        column = [p[i] for p in self.pixels]
        column = column[-n:] + column[:-n]
        for row in range(len(self.pixels)):
            self.pixels[row][i] = column[row]

    def lit_pixel_count(self):
        return len([p for row in self.pixels for p in row if p == '#'])

    def __repr__(self):
        return '\n'.join([''.join(p) for p in self.pixels])

if __name__ == '__main__':
    screen = Screen(50, 6)
    with open('8-input.txt', 'r') as f:
        data = f.read()
    lines = data.strip().split('\n')
    for line in lines:
        rect = re.match(u'rect (\d+)x(\d)', line)
        if rect is not None:
            screen.create_rect(*rect.groups())
            continue

        rotate_row_data = re.match(u'rotate row y=(\d+) by (\d+)', line)
        if rotate_row_data is not None:
            screen.rotate_row(*rotate_row_data.groups())
            continue

        rotate_column_data = re.match(u'rotate column x=(\d+) by (\d+)', line)
        if rotate_column_data is not None:
            screen.rotate_column(*rotate_column_data.groups())


    print('\nSCREEN:\n%s' % screen)
    print('\nLit pixel count: %d' % (screen.lit_pixel_count()))
