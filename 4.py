import re

if __name__ == '__main__':
    sector_sum = 0
    with open('4-input.txt', 'r') as f:
        data = f.read()
    lines = data.strip().split('\n')
    for line in lines:
        parts = line.split('-')
        char_counts = {}
        for part in parts:
            if re.match(r'[a-z]', part[0]) is not None:
                for char in part:
                    char_counts[char] = char_counts.get(char, 0) + 1
            else:
                groups = re.match(r'(\d*)\[(\w{5})\]', part)
                sector_id = groups.group(1)
                checksum = groups.group(2)
                ordered_counts= sorted(
                    sorted(char_counts.items(), key=lambda x: x[0]),
                    key=lambda x: x[1], reverse=True
                )
                ordered_list = [item[0] for item in ordered_counts]
                test_checksum = ''.join(ordered_list[:5])
                if test_checksum == checksum:
                    sector_sum += int(sector_id)

    print sector_sum
