import re

if __name__ == '__main__':
    with open('9-input.txt', 'r') as f:
        data = f.read()

    uncompressed = ''
    for i in range(1000):
        #print '\n'
        #print data
        #print uncompressed
        data_match = re.search(r'^(?!\()(.*?)(?:\(|$)', data)
        if data_match is not None:
            raw_data = data_match.group(1)
            uncompressed = uncompressed + raw_data
            data = data[len(raw_data):]
            continue

        marker_match = re.search(r'\((\d+)x(\d+)\)', data)
        if marker_match is None:
            break

        (offset, multiplier) = marker_match.groups()
        data_offset = 3 + len(offset) + len(multiplier)
        for m in range(int(multiplier)):
            uncompressed += data[data_offset:data_offset + int(offset)]
        data = data[data_offset + int(offset):]

    print(len(uncompressed))
    #print data
    #print uncompressed
