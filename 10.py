import re

class Botbalance:
    def __init__(self, instructions):
        self.instructions = instructions
        self.actions = {}
        self.bots = {}
        self.outputs = {}

    def give_chips(self, bot):
        if len(self.bots[bot]) == 2:
            values = self.bots[bot]
            low_value = min(values)
            high_value = max(values)
            self.bots[bot] = []
            if 17 in values and 61 in values:
                print('Problem 1: %s' % bot)
            (_, low_type, low, high_type, high) = self.actions[bot]
            if low_type == 'bot':
                low_bot = self.bots.get(low, [])
                low_bot.append(low_value)
                self.bots[low] = low_bot
                self.give_chips(low)
            else:
                low_output = self.outputs.get(low, [])
                low_output.append(low_value)
                self.outputs[low] = low_output

            if high_type == 'bot':
                high_bot = self.bots.get(high, [])
                high_bot.append(high_value)
                self.bots[high] = high_bot
                self.give_chips(high)
            else:
                high_output = self.outputs.get(high, [])
                high_output.append(high_value)
                self.outputs[high] = high_output

    def run(self):
        for instruction in self.instructions:
            match = re.findall(r'\d+', instruction)
            if len(match) == 2:
                (value, bot) = match
                values = self.bots.get(bot, [])
                values.append(int(value))
                self.bots[bot] = values
            else:
                action = re.match(r'.*?(\d+) gives low to (.*?) (\d+) and high to (.*?) (\d+)', instruction)
                bot = action.group(1)
                self.actions[bot] = action.groups()

        for bot in self.bots.keys():
            self.give_chips(bot)



if __name__ == '__main__':
    with open('10-input.txt', 'r') as f:
        data = f.read()
    lines = data.strip().split('\n')
    bb = Botbalance(lines)
    bb.run()
    print('Problem 2: %d' % (bb.outputs['0'][0] * bb.outputs['1'][0] * bb.outputs['2'][0]))
